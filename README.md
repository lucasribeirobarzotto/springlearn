# learnspring #

A simple project for me to learn Spring.

### What is covered? ###

* Spring Context
* AOP
* Transactions
* MySQL
* Docker
* SpringMVC
* Tomcat