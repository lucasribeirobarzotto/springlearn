package com.lucas.domain;

import java.util.Objects;

public class Market {

    private Integer id;
    private String name;
    private Integer totalPrice;

    public Market() {
    }

    public Market(Integer id, String name, Integer totalPrice) {
        this.id = id;
        this.name = name;
        this.totalPrice = totalPrice;
    }

    public Market(String name) {
        this.name = name;
        this.totalPrice = 0;
        this.id = null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Market market = (Market) o;
        return Objects.equals(name, market.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return "Market{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", totalPrice=" + totalPrice +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Integer totalPrice) {
        this.totalPrice = totalPrice;
    }
}
