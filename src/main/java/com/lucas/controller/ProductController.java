package com.lucas.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lucas.domain.Product;
import com.lucas.service.StockService;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

import static org.springframework.util.MimeTypeUtils.APPLICATION_JSON_VALUE;

@Controller
public class ProductController {

    private StockService stockService;

    public ProductController(StockService stockService) {
        this.stockService = stockService;
    }

    @GetMapping("/product/{productId}")
    public String getProductById(@PathVariable Integer productId, Model model) {

        Product product = stockService.getProductFromStockById(productId);

        if (product == null) {
            return "productNotFound.html";
        } else {
            model.addAttribute("product", product);
            return "productFound.html";
        }

    }

    @PostMapping(value = "/newproduct", consumes = APPLICATION_JSON_VALUE)
    public String addNewProduct(HttpEntity<String> httpEntity, Model model) throws JsonProcessingException {

        String json = httpEntity.getBody();

        Product product = new ObjectMapper().readValue(json, Product.class);

        stockService.addNewProductToStock(product);
        model.addAttribute("product", product);

        return "productAdded.html";
    }

    @GetMapping("/allproducts")
    public String getAllProducts(Model model) {

        List<Product> productList = stockService.getAllProductsFromStock();

        model.addAttribute("productList", productList);

        return "allProducts.html";
    }

}