package com.lucas.controller;

import com.lucas.domain.Market;
import com.lucas.service.MarketService;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class MarketController {

    private MarketService marketService;

    public MarketController(MarketService marketService) {
        this.marketService = marketService;
    }

    @PostMapping("/newmarket")
    public Market addNewMarket(@RequestBody Market market) {
        marketService.createNewMarket(market);
        return market;
    }

    @GetMapping("/allmarkets")
    public List<Market> getAllMarkets(Model model) {
        List<Market> marketList = marketService.getAllMarkets();

        model.addAttribute("marketList", marketList);

        return marketList;
    }

}
