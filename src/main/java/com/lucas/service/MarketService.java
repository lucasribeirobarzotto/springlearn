package com.lucas.service;

import com.lucas.dao.MarketDAO;
import com.lucas.dao.MarketStockDAO;
import com.lucas.dao.StockDAO;
import com.lucas.domain.Market;
import com.lucas.domain.Product;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class MarketService {

    private MarketDAO marketDAO;
    private StockDAO stockDAO;
    private MarketStockDAO marketStockDAO;

    public MarketService(MarketDAO marketDAO, StockDAO stockDAO, MarketStockDAO marketStockDAO) {
        this.marketDAO = marketDAO;
        this.stockDAO = stockDAO;
        this.marketStockDAO = marketStockDAO;
    }

    public void createNewMarket(Market market) {
        marketDAO.add(market);
    }

    public List<Market> getAllMarkets() {
        return marketDAO.getAll();
    }

    public Market getMarketById(Integer id) {
        return marketDAO.getById(id);
    }

    public void deleteMarket(Integer id) {
        marketDAO.delete(id);
    }

    public String addProductToMarket(Market market, Product product) {
        Integer id_product = checkIfStockContainsProduct(product);
        Integer id_market = checkIfMarketExists(market);
        if (id_product == null) return "Sorry, product out of stock!";
        if (id_market == null) return "Sorry, market doesn't exists!";
        marketStockDAO.addProductToMarket(id_market, id_product);
        return "Product added successfully!";
    }

    public List<Product> getProductsOfMarket(Market market) {
        Integer id_market = checkIfMarketExists(market);
        if (id_market == null) return null;
        List<Integer> productIdList = marketStockDAO.getListOfProductIdsFromMarket(id_market);
        List<Product> productList = new ArrayList<>();
        for (Integer id : productIdList) {
            productList.add(stockDAO.getById(id));
        }
        return productList;
    }

    private Integer checkIfMarketExists(Market market) {
        List<Market> marketList = marketDAO.getAll();
        Integer id_market = null;
        for (Market eachMarket : marketList) {
            if (eachMarket.equals(market)) {
                id_market = eachMarket.getId();
                break;
            }
        }
        return id_market;
    }

    private Integer checkIfStockContainsProduct(Product product) {
        List<Product> productList = stockDAO.getAll();
        Integer id_product = null;
        for (Product eachProd : productList) {
            if (eachProd.equals(product)) {
                id_product = eachProd.getId();
                break;
            }
        }
        return id_product;
    }
}
