package com.lucas.service;

import com.lucas.dao.StockDAO;
import com.lucas.domain.Product;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class StockService {

    private StockDAO stockDAO;

    public StockService(StockDAO stockDAO) {
        this.stockDAO = stockDAO;
    }

    public void addNewProductToStock(Product product) {
        stockDAO.add(product);
    }

    public List<Product> getAllProductsFromStock() {
        return stockDAO.getAll();
    }

    public Product getProductFromStockById(Integer id) {
        try {
            return stockDAO.getById(id);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    public void deleteProductFromStock(Integer id) {
        stockDAO.delete(id);
    }
}
