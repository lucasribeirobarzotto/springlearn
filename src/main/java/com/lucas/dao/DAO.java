package com.lucas.dao;

import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface DAO<A> {

    A getById(Integer id);

    void add(A a);

    List<A> getAll();

    void delete(Integer id);

}
