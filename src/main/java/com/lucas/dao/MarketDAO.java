package com.lucas.dao;

import com.lucas.domain.Market;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Component
public class MarketDAO implements DAO<Market> {

    private JdbcTemplate jdbc;

    public MarketDAO(JdbcTemplate jdbc) {
        this.jdbc = jdbc;
    }

    @Override
    public Market getById(Integer id) {
        String sql = "SELECT * FROM MARKETS WHERE id = ?";
        Market market = jdbc.queryForObject(
                sql,
                (rs, rowNum) -> new Market(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getInt(3)
                ),
                new Object[]{id});
        return market;
    }

    @Override
    public void add(Market market) {
        String sql = "INSERT INTO MARKETS (name, totalPrice) VALUES (?, ?)";
        jdbc.update(sql, market.getName(), market.getTotalPrice());
    }

    @Override
    public List<Market> getAll() {
        String sql = "SELECT * FROM MARKETS";

        List<Market> marketList = jdbc.query(
                sql,
                new RowMapper<Market>() {
                    @Override
                    public Market mapRow(ResultSet rs, int i) throws SQLException {
                        Market market = new Market(rs.getInt(1), rs.getString(2), rs.getInt(3));
                        return market;
                    }
                });

        return marketList;
    }

    @Override
    public void delete(Integer id) {
        String sql = "DELETE FROM MARKETS WHERE id = ?";
        jdbc.update(sql, new Object[]{id});
    }
}
