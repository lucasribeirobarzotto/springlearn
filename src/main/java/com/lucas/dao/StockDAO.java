package com.lucas.dao;

import com.lucas.domain.Product;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Component
public class StockDAO implements DAO<Product> {

    private JdbcTemplate jdbc;

    public StockDAO(JdbcTemplate jdbc) {
        this.jdbc = jdbc;
    }

    @Override
    public Product getById(Integer id) {
        String sql = "SELECT * FROM STOCK WHERE id = ?";
        Product product = jdbc.queryForObject(
                sql,
                (rs, rowNum) -> new Product(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getInt(3)
                ),
                new Object[]{id});
        return product;
    }

    @Override
    public void add(Product product) {
        String sql = "INSERT INTO STOCK (name, price) VALUES (?, ?)";
        jdbc.update(sql, product.getName(), product.getPrice());
    }

    @Override
    public List<Product> getAll() {
        String sql = "SELECT * FROM STOCK";

        List<Product> productList = jdbc.query(
                sql,
                new RowMapper<Product>() {
                    @Override
                    public Product mapRow(ResultSet rs, int i) throws SQLException {
                        Product product = new Product(rs.getInt(1), rs.getString(2), rs.getInt(3));
                        return product;
                    }
                });

        return productList;
    }

    @Override
    public void delete(Integer id) {
        String sql = "DELETE FROM STOCK WHERE id = ?";
        jdbc.update(sql, new Object[]{id});
    }
}
