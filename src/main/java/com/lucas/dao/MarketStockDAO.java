package com.lucas.dao;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Component
public class MarketStockDAO {

    private JdbcTemplate jdbc;

    public MarketStockDAO(JdbcTemplate jdbc) {
        this.jdbc = jdbc;
    }


    public void addProductToMarket(Integer id_market, Integer id_product) {
        String sql = "INSERT INTO MARKETS_STOCK (id_market, id_product) VALUES (?, ?)";
        jdbc.update(sql, id_market, id_product);
    }

    public List<Integer> getListOfProductIdsFromMarket(Integer id) {
        String sql = "SELECT * FROM MARKETS_STOCK WHERE id_market = ?";

        List<Integer> productIdList = jdbc.query(
                sql,
                new RowMapper<Integer>() {
                    @Override
                    public Integer mapRow(ResultSet rs, int i) throws SQLException {
                        return rs.getInt(2);
                    }
                },
                new Object[]{id});

        return productIdList;
    }

}
